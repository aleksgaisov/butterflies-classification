# Butterflies classification

The project utilizes the [PyTorch](https://pytorch.org/) library in order to classify the 75 species of butterflies. 

## Usage

The python code is written within the [Jupyter Notebook](https://jupyter.org/) and, therefore, should be executed accordingly.
[Install](https://jupyter.org/install) Jupyter Notebook on your system and open the `butterflies-classification.ipynb` file.

The [data set](https://www.kaggle.com/datasets/gpiosenka/butterfly-images40-species) needs to be downloaded as well. 
The path to the folder with sub directories (butterlfies species) needs to be specified within the code for both: training and testing data set.

## Explanation

[This acticle](https://towardsdatascience.com/handwritten-digit-mnist-pytorch-977b5338e627) was used to get a basic understanding of how to work with the given library. 

With the help of PyToch, the program is capable of:

* Transforming and loading the images from the dataset
* Selecting GPU (if avaliable) as a processing device
* Defining and training the neural network (predictions are assessed via NLL function)
* Testing and saving the model 

Overall it provides a decent framework for simple image classification projects.

